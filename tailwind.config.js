module.exports = {
  content: [
    'components/**/*.{js,vue,ts}',
    'layouts/**/*.vue',
    'pages/**/*.vue',
    'plugins/**/*.{js,ts}',
    'nuxt.config.{js,ts}',
  ],
  prefix: 'tw-',
  important: true,
  theme: {
    extend: {
    },
    darkSelector: ".dark-mode"
  },
  variants: {
    backgroundColor: [
      "dark",
      "dark-hover",
      "dark-group-hover",
      "dark-even",
      "dark-odd",
      "hover",
      "responsive"
    ],
    borderColor: [
      "dark",
      "dark-focus",
      "dark-focus-within",
      "hover",
      "responsive"
    ],
    textColor: ["dark", "dark-hover", "dark-active", "hover", "responsive"]
  },
  plugins: [require('autoprefixer'), require('@tailwindcss/typography'),require('tailwindcss-dark-mode')()],
}

